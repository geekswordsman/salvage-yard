export const CONFIG = {};

CONFIG.ID = 'salvage-yard';

CONFIG.FRIENDLY_NAME = 'Salvage Yard';

CONFIG.SETTINGS = {
    ENABLED_TYPES: 'enabled_types'
}

CONFIG.TEMPLATES = {
    CONFIG: "modules/salvage-yard/templates/salvageyardConfig.hbs"
}

CONFIG.DOCUMENT_TYPES = {
    "ACTOR": {
        name: "Actor",
        enabled: true,
        packName: "Actors"
    },
    "CARDS": {
        name: "Cards",
        enabled: true,
        packName: "Cards"
    },
    "ITEM": {
        name: "Item",
        enabled: true,
        packName: "Items"
    },
    "JOURNALENTRY": {
        name: "JournalEntry",
        enabled: true,
        packName: "JournalEntries"
    },
    "JOURNALENTRYPAGE": {
        name: "JournalEntryPage",
        enabled: true,
        packName: "JournalEntries"
    },
    "MACRO": {
        name: "Macro",
        enabled: true,
        packName: "Macros"
    },
    "PLAYLIST": {
        name: "Playlist",
        enabled: true,
        packName: "Playlists"
    },
    "ROLLTABLE": {
        name: "RollTable",
        enabled: true,
        packName: "RollTables"
    },
    "SCENE": {
        name: "Scene",
        enabled: true,
        packName: "Scenes"
    }
}
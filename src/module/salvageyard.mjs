import { CONFIG } from "./config.mjs";
import { SalvageYardConfig } from "./forms/salvageyardConfig.mjs";
import { Logger } from "./geekdialog/geekdialogs.mjs";

export class SalvageYard {
    static initialize() {
        //Register settings
        Logger.log(true, "Initializing Salvage Yard...");
        game.settings.register(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES, {
            config: false,
            default: CONFIG.DOCUMENT_TYPES,
            scope: 'world',
            type: Object
        });

        game.settings.registerMenu(CONFIG.ID, "ModuleConfiguration", {
            name: "SalvageYardConfiguration",
            label: game.i18n.localize("SALVAGEYARD.CONFIG.LABEL"),
            hint: game.i18n.localize("SALVAGEYARD.CONFIG.HINT"),
            icon: "fas fa-cog",
            type: SalvageYardConfig,
            restricted: true
        });

        //Get Settings
        const settings = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES);
        for (let key of Object.keys(CONFIG.DOCUMENT_TYPES)) {
            if (settings[key]) {
                CONFIG.DOCUMENT_TYPES[key].enabled = settings[key].enabled;
            }
        }
    }

    static async ready() {
        if (!game.user.isGM) { return; }
        
        Hooks.on('preDeleteFolder', (document, options, userId) => { SalvageYard.SalvageFolder(document, options, userId); });
        
        const folderName = game.i18n.localize("SALVAGEYARD.SALVAGEYARD");
        let salvageFolder = game.folders.find(f => f.name === folderName && f.type === "Compendium");;
        if (!salvageFolder) {
            salvageFolder = await Folder.create({name: folderName, type: "Compendium"});
        }

        //check if compendiums exist, create if necessary
        const types = Object.keys(CONFIG.DOCUMENT_TYPES);
        for (let t of types) {
            //Attach On Delete Hooks
            if (CONFIG.DOCUMENT_TYPES[t].enabled) {
                Hooks.on(`preDelete${CONFIG.DOCUMENT_TYPES[t].name}`, (document, options, userId) => { SalvageYard.SalvageDocument(CONFIG.DOCUMENT_TYPES[t], document, options, userId); });
            }

            let lowerPackName = `${CONFIG.ID}_${CONFIG.DOCUMENT_TYPES[t].packName}`.toLowerCase();
            if (!game.packs.get(`world.${lowerPackName}`)) {
                let comp = await CompendiumCollection.createCompendium({
                    "label": `${CONFIG.FRIENDLY_NAME} ${CONFIG.DOCUMENT_TYPES[t].packName}`,
                    "type": CONFIG.DOCUMENT_TYPES[t].name,
                    "name": lowerPackName,
                    "package": CONFIG.ID,
                    "path": "packs/" + lowerPackName + ".db"
                });

                if (!salvageFolder) {
                    salvageFolder = game.folders.find(f => f.name === folderName && f.type === "Compendium");
                }

                await comp.setFolder(salvageFolder);
            }
        }
    }

    static async SalvageDocument(salvageType, document, options, userId) {
        Logger.debug(false, salvageType, document, options, userId);
        if (!salvageType.enabled) { return; }

        let lowerPackName = `world.${CONFIG.ID}_${salvageType.packName}`.toLowerCase();
        if (document.pack === lowerPackName) { return; } // We don't salvage from the salvage yard

        try {
            let pack = game.packs.get(lowerPackName);
            if (salvageType.name === "JournalEntryPage") {
                let deletedJournal = await pack.getName("Deleted Pages");
                if (!deletedJournal) {
                    deletedJournal = new JournalEntry({"name": "Deleted Pages"});
                    deletedJournal = await pack.importDocument(deletedJournal);
                }

                await deletedJournal.createEmbeddedDocuments("JournalEntryPage", [document], {});
            } else {
                pack.importDocument(document, {keepId: true});
            }
    
            Logger.log(true, `Salvage Yard just salvaged a document with id ${document.id}!`);
        } catch (e) {
            console.error("Error attempting to salvage a document!", e);
            Logger.log(true, "Salvage Yard Error Information", salvageType, document, options, userId);
        }
    }

    static async SalvageFolder(document, options, userId) {
        let salvageType = CONFIG.DOCUMENT_TYPES[document.type.toUpperCase()];
        if (!salvageType.enabled) { return; }

        Logger.log(true, `Salvage Yard detected the deletion of folder ${document.name} with id ${document.id}, salvaging documents...`);

        if (document.children.length) {
            for (let child of document.children) {
                await SalvageYard.SalvageFolder(child, options, userId);
            }
        }

        let lowerPackName = `world.${CONFIG.ID}_${salvageType.packName}`.toLowerCase();
        try {
            let pack = game.packs.get(lowerPackName);
            for (let d of document.content) {
                pack.importDocument(d, {keepId: true});
                Logger.log(true, `Salvage Yard just salvaged a document with id ${d.id}!`);
            }
        } catch (e) {
            console.error("Error attempting to salvage a document!", e);
            Logger.error(true, "Salvage Yard Error Information", salvageType, document, options, userId);
        }
    }
}

# salvage-yard

The Salvage Yard - holds on to deleted documents to prevent catastrophy.

## What?

If you're worried about accidentally deleting data in Foundry and losing it forever, use The Salvage Yard!

The Salvage Yard works by creating a set of compendiums - one for each document type (Scene, Actor, Item, Journal, Playlist, Rolltable, and CardDeck).  It then hooks into Foundry and detects when one of these documents gets deleted - and stores a copy into the appropriate compendium!  It retains all of the original information, meaning if you need to recover something, you can simply open the relevant compendium and import it back into your world.

As an optional feature, The Salvage Yard can even detect when you are deleting things out of another compendium, and back those up as well!

## Usage

Enable the module.

By default, it will create world compendiums for each document type, and anytime you delete a document from anywhere, it will automatically store a copy of that document.

The module configuration window will allow you to control what document types you don't want to automatically archive.  It also provides some quick information on the size of Salvage Yards compendiums along with a button to purge any one of those compendiums, deleting all of the contents.

Please note that Salvage Yard only works for GMs and Assistant GM's; items deleted a character sheet by a player will not be saved!
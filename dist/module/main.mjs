/* Imports */
import { CONFIG } from './config.mjs';
import { SalvageYard } from './salvageyard.mjs';
import { Logger, AboutDialog } from './geekdialog/geekdialogs.mjs';

Logger.MODULE_ID = AboutDialog.MODULE_ID = CONFIG.ID;

Hooks.once('init', () => { SalvageYard.initialize(); });
Hooks.once('ready', () => { SalvageYard.ready(); });

/* Enable Debug Module */
Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
    registerPackageDebugFlag(CONFIG.ID);
});
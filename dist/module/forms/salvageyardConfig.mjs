import { CONFIG } from "../config.mjs";
import { HelpFormApplication, Logger } from "../geekdialog/geekdialogs.mjs";

export class SalvageYardConfig extends HelpFormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;

        const overrides = {
            id: "salvageyardConfig",
            template: CONFIG.TEMPLATES.CONFIG,
            submitOnChange: true,
            closeOnSubmit: false,
            width: 500,
            height: "auto",
            tabs: [{ navSelector: ".tabs", contentSelector: "#salvageyardDialogForm", initial: "config" }],
            title: game.i18n.localize("SALVAGEYARD.CONFIG.LABEL")
        };

        return foundry.utils.mergeObject(options, overrides);
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', "[data-action]", this._handleButtonClick.bind(this));
    }

    _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;

        let _this = this;

        switch (action) {
            case "purge":
                const docType = clickedElement.data().doctype;
                Logger.debug(false, docType);
                let packName = CONFIG.DOCUMENT_TYPES[docType].packName;
                let documentType = game.i18n.localize(`DOCUMENT.${packName}`);

                let content = `<p>${game.i18n.localize("SALVAGEYARD.PURGE.DOCUMENT")} ${documentType}</p><p>${game.i18n.localize("SALVAGEYARD.PURGE.WARNING")}`;
                new Dialog({
                    title: game.i18n.localize("SALVAGEYARD.PURGE.TITLE"),
                    content: content,
                    buttons: {
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("Cancel")
                        },
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("SALVAGEYARD.CONFIG.PURGE"),
                            callback: (html) => {
                                _this.purgeDocuments(docType);
                            }
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            default:
                break;
        }

        return true;
    }

    async getData(options) {
        let settings = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES);

        //Get pack information
        function formatBytes(a,b=2){if(0===a)return"0 Bytes";const c=0>b?0:b,d=Math.floor(Math.log(a)/Math.log(1024));return parseFloat((a/Math.pow(1024,d)).toFixed(c))+" "+["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"][d]};

        let packInfo = {}
        for (let key of Object.keys(settings)) {
            if (key === "JOURNALENTRYPAGE") { continue; }
            settings[key].localizedName = game.i18n.localize(`DOCUMENT.${settings[key].packName}`);
            let lowerPackName = `world.${CONFIG.ID}_${settings[key].packName}`.toLowerCase();
            let pack = game.packs.get(lowerPackName);
            let packContents = await pack.getIndex();

            packInfo[key] = {
                name: settings[key].localizedName,
                count: packContents.size,
                indexSize: formatBytes(JSON.stringify(packContents.contents).length)
            }
        }

        return { settings, packInfo };
    }

    async _updateObject(event, formData) {
        let settings = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES);
        let keys = Object.keys(formData);
        for (let key of keys) {
            settings[key].enabled = formData[key];
        }

        await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.ENABLED_TYPES, settings);
    }

    async purgeDocuments(docType) {
        let packName = `world.${CONFIG.ID}_${CONFIG.DOCUMENT_TYPES[docType].packName}`.toLowerCase();
        let pack = game.packs.get(packName);
        let indexed = await pack.getIndex();
        for (let k of indexed.contents) {
            let doc = await pack.getDocument(k._id);
            await doc.delete();
        }

        this.render(true);
    }
}
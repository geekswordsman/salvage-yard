# Salvage Yard
## v1.1.1 - 2024-06-26
 - Migrated code to v12
 - Re-arranged source to automate build process
 - Migrated CSS to LESS
 - Added About/Form and Logger dialogs from my shared repo
 
## v1.1.0 - 2023-06-13
 - Migrated code to v11
 - Added ability to reclaim deleted Journal Pages ( #4 )
 - Taking advantage of Compendium Folders to organize Salvage Yard Compendiums for fresh installs
 - Styling and code cleanup

## v1.0.5 - 2022-11-08
 - Hot patch to fix initialization bug introduced in v1.0.4
 - Merged request to restrict access to module configuration to only GM's

## v1.0.4 - 2022-11-07
 - Changed behavior due to an issue where players deleting items would result in an error about them not having permissions to modify a compendium; due to changes in v10 this is a necessary step as it is not possible to grant them access.

## v1.0.3 - 2022-07-21
 - FoundryVTT v10 Compatibility Update
 
## v1.0.2 - 2022-03-31
 - Removed salvage-yard.lock from repo, which...was never supposed to be there.  Sorry folks.
 
## v1.0.1 - 2022-03-17
 - Fixed issue with documents within a folder not getting salvaged when that folder (or parent folders) were being deleted, due to Foundry workflow not firing the preDelete hooks on these documents.

## v1.0.0 - 2022-03-06
- First release of Salvage Yard!